DECLARE

/****************************************************************************** 
 * 
 * <Title> - CB001S020 - Story2515 - StreamFinalBillNumbers
 *
 * 
 * Version Author  Date       Comments
 * ------- ------- ---------- -------------------------------------------------
 * 
 * 
 * Usage: @?? - no parms
 * 
 * 
 *****************************************************************************/

  e_mess  CHAR(80);

  TYPE TABLE_ice_customer_id    IS TABLE OF bill.ice_customer_id%TYPE INDEX BY BINARY_INTEGER;
  T_ice_customer_id             TABLE_ice_customer_id          ;

  v_start_fg                    boolean := false;
  v_range_start                 cb8_cand_with_prod_lt_ret_dt.ICE_CUSTOMER_ID%TYPE := 0;
  v_range_end                   cb8_cand_with_prod_lt_ret_dt.ICE_CUSTOMER_ID%TYPE := 0;
  v_range_end_count             number :=0;
  v_stored_ice_customer_id      cb8_cand_with_prod_lt_ret_dt.ICE_CUSTOMER_ID%TYPE := 0;
  
  /* Retrieve all */ 

  CURSOR c_Main IS
    SELECT
       ice_customer_id
       FROM cb8_cand_with_prod_lt_ret_dt prods
	   order by ice_customer_id
       ;


BEGIN

  DBMS_OUTPUT.PUT_LINE('start');
  
  e_mess := 'Open main cursor';
  OPEN  c_Main;

  e_mess := 'Bulk collect main cursor';
  FETCH c_Main BULK COLLECT INTO
     T_ice_customer_id
  ;
  
  DBMS_OUTPUT.PUT_LINE('cursor count = '||T_ice_customer_id.COUNT);

  v_range_end_count := 1000000;
  
  e_mess := 'Start of main loop';
  FOR i IN 1..T_ice_customer_id.COUNT LOOP
  --FOR i IN 1..10000 LOOP
      
    if i = 1 then
      v_range_start := T_ice_customer_id(i);
    end if;
    
    if v_start_fg then
      v_range_start := T_ice_customer_id(i);
      v_start_fg := false;
    end if;

    if i = v_range_end_count then
      v_range_end := T_ice_customer_id(i);
      DBMS_OUTPUT.PUT_LINE('       --and prods.ice_customer_id between ' || v_range_start || ' and ' || v_range_end );
      v_range_end_count := v_range_end_count + 1000000;
      v_start_fg := true;
    end if;
    
    v_stored_ice_customer_id := T_ice_customer_id(i);
      
  END LOOP;
  
  DBMS_OUTPUT.PUT_LINE('       --and prods.ice_customer_id between ' || v_range_start || ' and ' || v_stored_ice_customer_id );
 
  e_mess := 'Close main cursor';
  CLOSE c_Main;

  EXCEPTION
     WHEN OTHERS
     THEN e_mess := SUBSTR(SQLERRM,1,80);
          DBMS_OUTPUT.PUT_LINE('******************************************************************* ');
          DBMS_OUTPUT.PUT_LINE('* ');
          DBMS_OUTPUT.PUT_LINE('* ERROR: '||e_mess);
          DBMS_OUTPUT.PUT_LINE('* ');
          DBMS_OUTPUT.PUT_LINE('* Work Rolled Back - No date change applied');
          DBMS_OUTPUT.PUT_LINE('* ');
          DBMS_OUTPUT.PUT_LINE('******************************************************************* ');
          ROLLBACK;


END; 
/


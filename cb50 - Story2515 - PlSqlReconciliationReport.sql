DECLARE

/****************************************************************************** 
 * 
 * <Title> - CB001S050 - Story2515 - PlSqlReconciliationReport
 *
 * 
 * Version Author  Date       Comments
 * ------- ------- ---------- -------------------------------------------------
 * 2.0             21/03/18   for exceptions: only show amts not in other 
 *                            exceptions
 * 
 * Usage: @?? - no parms
 * 
 * 
 *****************************************************************************/

----------------------------------------------------------------------------
--  NOTE: This script has been written with far too much "decision" code
--        within it. The decisions should have been handled by using tables
--        in the previous script (cb40).
--        The reason for the above was due to the Requirements constantly 
--        changing
----------------------------------------------------------------------------

  e_mess  CHAR(80);

  v_reconciled_fg                         boolean := true;
  v_dateTime                              timestamp;

  -- Reconcile Age - variables
  v_customer_cnt                          number; 
  v_cb1_cand_cust_lt_ret_dt_cnt           number;
  v_cb2_cand_cust_gt_ret_dt_cnt           number;
  v_wk_ic_age_chkpt                       number;
  v_age_reconciled                        boolean := true;
  
  -- Reconcile Inactive Prods - variables
  v_wk_ic_inactive_prod_chkpt             number;
  v_cb8_cand_with_prod_lt_ret_dt          number;

  -- Reconcile Final Billed - variables
  v_wk_ic_final_billed_chkpt              number;
  v_cb18a_cand_fb_ready_for_del           number;
  v_cb17a_cand_without_any_bills          number;

  -- Reconcile Shell - variables
  v_wk_ic_shell_account_chkpt             number;
  v_cb3_cand_with_shell                   number;

  -- Reconcile Orphaned - variables
  v_cb31_custs_with_orphan_accts          number;
  v_wk_ic_orphan_account_chkpt            number;

  -- Reconcile Zero Balance - variables
  v_wk_ic_current_bal_chkpt               number; 
  v_cb22_cand_without_bal_zero            number;
  v_wk_ic_current_bal_chkpt_tmp1          number;
  v_wk_ic_current_bal_chkpt_tmp2          number;

  -- Reconcile Dormant List - variables
  v_wk_ic_dormant_list                    number;
  v_cb27_cand_final_list                  number;

  -- Reconcile Cancelled - variables
  v_wk_ic_cancelled_chkpt                 number; 
  v_cb12_cand_with_all_cancelled          number; 
  
  -- Reconcile Telco - variables
  v_wk_ic_telco_chkpt                     number; 
  v_cb15_cand_with_all_telco              number; 

  -- Legal Case - variables
  v_WK_IC_LC_EXCLUSION                    number;
  v_CB_31_IC_LC_EXCLUSION                 number;
  v_cb_LC_EXCLUSION                       number;
  
  -- Agent Manager Visit - variables
  v_WK_IC_DCA_CUSTOMER_CHKPT              number;
  v_cb30_cand_with_agt_mgr_visit          number;

  -- Last Action - variables
  v_WK_IC_LACT_CUSTOMER_CHKPT             number;
  v_cb_LT_CUSTOMER                        number;
  
  -- Long Term Pay Arrangement - variables
  v_WK_IC_LONG_ARRANGEMENT_CHKPT          number;
  v_cb_LTP_Accounts                       number;

  -- Long Term Pay Arrangement - variables
  v_WK_IC_LACT_CUST_HIST_CHKPT            number;
  v_cb_Hist_laa_Accounts                  number;

 
  
BEGIN

  begin SELECT CURRENT_TIMESTAMP into v_dateTime FROM DUAL; end;
  DBMS_OUTPUT.PUT_LINE('GDPR Existing - Reconciliation Report - Date Run:  ' || v_dateTime);
  DBMS_OUTPUT.PUT_LINE('');

  DBMS_OUTPUT.PUT_LINE('==============================================================================');
  DBMS_OUTPUT.PUT_LINE('====== Build Dormancy List                                              ======');
  DBMS_OUTPUT.PUT_LINE('==============================================================================');
  DBMS_OUTPUT.PUT_LINE('');

  --===============================================================================
  DBMS_OUTPUT.PUT_LINE('-- Reconcile Inactive Products --');
  
  DBMS_OUTPUT.PUT_LINE('');
  e_mess := 'reconcile inactive prods';
  v_reconciled_fg := true;

  begin
    select count(*) into v_wk_ic_inactive_prod_chkpt
    from wk_ic_age_chkpt wk1
    ;
    select count(*) into v_cb8_cand_with_prod_lt_ret_dt from cb8_cand_with_prod_lt_ret_dt;
  EXCEPTION
     WHEN OTHERS
     THEN e_mess := SUBSTR(SQLERRM,1,80);
  END;

  -- check retrieved inactive prods lt retention date equals DRP 
  if v_wk_ic_inactive_prod_chkpt <> v_cb8_cand_with_prod_lt_ret_dt then
    v_reconciled_fg := false;
  end if;
  
  if v_reconciled_fg = true then
    DBMS_OUTPUT.PUT_LINE('RESULT = reconciled');
  else
    DBMS_OUTPUT.PUT_LINE('RESULT = NOT RECONCILED');
  end if;


  DBMS_OUTPUT.PUT_LINE('-- Expected: Total Inactive Prods retrieved lt retention date:           '||v_cb8_cand_with_prod_lt_ret_dt);
  DBMS_OUTPUT.PUT_LINE('-- Actual:   Total DRP Inactive prods lt retention  date:                '||v_wk_ic_inactive_prod_chkpt);

  DBMS_OUTPUT.PUT_LINE('');
  DBMS_OUTPUT.PUT_LINE('');

  
  --===============================================================================
  DBMS_OUTPUT.PUT_LINE('-- Reconcile Final Billed --');
  DBMS_OUTPUT.PUT_LINE('');
  e_mess := 'reconcile final billed';
  v_reconciled_fg := true;

  begin
    select count(*) into v_wk_ic_final_billed_chkpt
    from wk_ic_final_billed_chkpt
    ;
  select count(*) into v_cb18a_cand_fb_ready_for_del from cb18_cand_fb_ready_for_del
  ;
  EXCEPTION
     WHEN OTHERS
     THEN e_mess := SUBSTR(SQLERRM,1,80);
  END;

  -- check retrieved final billed lt retention date equals DRP 
  if v_wk_ic_final_billed_chkpt <> v_cb18a_cand_fb_ready_for_del then
    v_reconciled_fg := false;
  end if;
  
  if v_reconciled_fg = true then
    DBMS_OUTPUT.PUT_LINE('RESULT = reconciled');
  else
    DBMS_OUTPUT.PUT_LINE('RESULT = NOT RECONCILED');
  end if;


  DBMS_OUTPUT.PUT_LINE('-- Expected: Total Final Billed retrieved lt retention date:             '||v_cb18a_cand_fb_ready_for_del);
  DBMS_OUTPUT.PUT_LINE('-- Actual:   Total DRP Final Billed lt retention  date:                  '||v_wk_ic_final_billed_chkpt);

  DBMS_OUTPUT.PUT_LINE('');
  DBMS_OUTPUT.PUT_LINE('');

  
  --===============================================================================
  DBMS_OUTPUT.PUT_LINE('-- Reconcile Cancelled (Not in Final Billed)--');
  DBMS_OUTPUT.PUT_LINE('');
  e_mess := 'reconcile cancelled';
  v_reconciled_fg := true;

  begin
     select count(*) into v_wk_ic_cancelled_chkpt
     from wk_ic_cancelled_chkpt
     where ice_customer_id not in
     (
     select ice_customer_id
     from wk_ic_final_billed_chkpt
     )
    ;
    select count(*) into v_cb12_cand_with_all_cancelled 
  from cb12_cand_with_all_cancelled
  where ice_customer_id not in
  (select ice_customer_id
  from cb18_cand_fb_ready_for_del
  );
  EXCEPTION
    WHEN OTHERS
       THEN e_mess := SUBSTR(SQLERRM,1,80);
  END;

  -- check retrieved ...
  if v_wk_ic_cancelled_chkpt <> v_cb12_cand_with_all_cancelled then
    v_reconciled_fg := false;
  end if;
  
  if v_reconciled_fg = true then
    DBMS_OUTPUT.PUT_LINE('RESULT = reconciled');
  else
    DBMS_OUTPUT.PUT_LINE('RESULT = NOT RECONCILED');
  end if;


  DBMS_OUTPUT.PUT_LINE('-- Expected: Total All Prods Cancelled + not in Exclusions / FB:         '||v_cb12_cand_with_all_cancelled);
  DBMS_OUTPUT.PUT_LINE('-- Actual:   Total DRP Custs with All Prods Cancelled:                   '||v_wk_ic_cancelled_chkpt);

  DBMS_OUTPUT.PUT_LINE('');
  DBMS_OUTPUT.PUT_LINE('');

  --===============================================================================
  DBMS_OUTPUT.PUT_LINE('-- Reconcile Telco (Zero Balanced and not in: FB or Cancelled) --');
  DBMS_OUTPUT.PUT_LINE('');
  e_mess := 'reconcile telco';
  v_reconciled_fg := true;

  begin
    select count(*) into v_wk_ic_telco_chkpt
    from wk_ic_telco_chkpt wk1, wk_ic_age_chkpt wk2  --, wk_ic_final_billed_chkpt fb 
    where wk1.ice_customer_id = wk2.ice_customer_id
  --and   wk1.ice_customer_id = fb.ice_customer_id
     and wk1.ice_customer_id not in
     (
     select ice_customer_id
     from wk_ic_final_billed_chkpt
     )
    ;
    select count(*) into v_cb15_cand_with_all_telco 
  from cb15_cand_with_all_telco tel --, cb18_cand_fb_ready_for_del fb
    --where tel.ice_customer_id = fb.ice_customer_id
    where tel.ice_customer_id NOT IN ( SELECT cb12.ice_customer_id FROM cb12_cand_with_all_cancelled cb12)
    and   tel.ice_customer_id NOT IN ( SELECT cb22a.ice_customer_id FROM cb22a_cand_without_bal_zero cb22a)
  and   tel.ice_customer_id not in
  (select ice_customer_id
  from cb18_cand_fb_ready_for_del
  )
  ;
  EXCEPTION
    WHEN OTHERS
       THEN e_mess := SUBSTR(SQLERRM,1,80);
  END;

  -- check retrieved ...
  if v_wk_ic_telco_chkpt <> v_cb15_cand_with_all_telco then
    v_reconciled_fg := false;
  end if;
  
  if v_reconciled_fg = true then
    DBMS_OUTPUT.PUT_LINE('RESULT = reconciled');
  else
    DBMS_OUTPUT.PUT_LINE('RESULT = NOT RECONCILED');
  end if;


  DBMS_OUTPUT.PUT_LINE('-- Expected: Total Custs with All Telco in Final Bill List:              '||v_cb15_cand_with_all_telco);
  DBMS_OUTPUT.PUT_LINE('-- Actual:   Total DRP Custs with All Telco in Final Bill List:          '||v_wk_ic_telco_chkpt);

  DBMS_OUTPUT.PUT_LINE('');
  DBMS_OUTPUT.PUT_LINE('');


--*******************************************************************************************************
--*******************************************************************************************************
--*******************************************************************************************************
  
  DBMS_OUTPUT.PUT_LINE('==============================================================================');
  DBMS_OUTPUT.PUT_LINE('====== Remove Exclusions from Dormancy List                             ======');
  DBMS_OUTPUT.PUT_LINE('==============================================================================');
  DBMS_OUTPUT.PUT_LINE('');

  DBMS_OUTPUT.PUT_LINE('-- Reconcile Non Zero Balance For Exclusion                              --');
  
  DBMS_OUTPUT.PUT_LINE('');
  e_mess := 'reconcile non zero balance';
  v_reconciled_fg := true;
  
  begin
    select count(*)
    into v_wk_ic_current_bal_chkpt_tmp1
    from drp.wk_ic_current_bal_chkpt wk
    where wk.ice_customer_id in
    (
    select wk1.ice_customer_id 
    from wk_ic_final_billed_chkpt wk1
    , wk_ic_age_chkpt wk3
    where wk1.ice_customer_id = wk3.ice_customer_id
    )
    ;
  
    select count(*)
    into v_wk_ic_current_bal_chkpt_tmp2
    from drp.wk_ic_current_bal_chkpt wk
    where wk.ice_customer_id in
    (
    select cb15.ice_customer_id 
    from cb15_cand_with_all_telco cb15
    )
    ;
  
    v_wk_ic_current_bal_chkpt := v_wk_ic_current_bal_chkpt_tmp1 + v_wk_ic_current_bal_chkpt_tmp2;

    -- this includes both telco + fb candidates
    select count(*) into v_cb22_cand_without_bal_zero from cb22a_cand_without_bal_zero;
  EXCEPTION
    WHEN OTHERS
       THEN e_mess := SUBSTR(SQLERRM,1,80);
  END;

  -- check retrieved ...
  if v_wk_ic_current_bal_chkpt <> v_cb22_cand_without_bal_zero then
    v_reconciled_fg := false;
  end if;
  
  if v_reconciled_fg = true then
    DBMS_OUTPUT.PUT_LINE('RESULT = reconciled');
  else
    DBMS_OUTPUT.PUT_LINE('RESULT = NOT RECONCILED');
  end if;


  DBMS_OUTPUT.PUT_LINE('-- Expected: Total Non Zero Balanced for exclusion:                      '||v_cb22_cand_without_bal_zero);
  DBMS_OUTPUT.PUT_LINE('-- Actual:   Total DRP Non Zero Balanced for exclusion:                  '||v_wk_ic_current_bal_chkpt);

  DBMS_OUTPUT.PUT_LINE('');
  DBMS_OUTPUT.PUT_LINE('');

  
   --===============================================================================
  DBMS_OUTPUT.PUT_LINE('-- Reconcile Agent Manager Visit (DCA) For Exclusion                     --');
  DBMS_OUTPUT.PUT_LINE('   (Total shown does not include DCA customers in Zero Bal List)  ');
  
  DBMS_OUTPUT.PUT_LINE('');
  e_mess := 'reconcile agent manager visit';
  v_reconciled_fg := true;

  begin
    select count(*)
    into v_WK_IC_DCA_CUSTOMER_CHKPT
    from WK_IC_DCA_CUSTOMER_CHKPT wk
    where wk.ice_customer_id in
    (
    select wk1.ice_customer_id 
    from cb26_cand_final_list_temp wk1
   )
     and wk.ice_customer_id not in
    (
    select ice_customer_id
    from cb_temp_wk_current_bal
    )
     ;

    select count(*) 
    into v_cb30_cand_with_agt_mgr_visit 
    from cb30_cand_with_agent_mgr_visit cb30
    where cb30.ice_customer_id in
    (
    select cb26.ice_customer_id 
    from cb26_cand_final_list_temp cb26
    )
    and cb30.ice_customer_id not in
    (
    select ice_customer_id
    from cb22a_cand_without_bal_zero
    )
    ;
  EXCEPTION
    WHEN OTHERS
       THEN e_mess := SUBSTR(SQLERRM,1,80);
  END;

  -- check retrieved ...
  if v_WK_IC_DCA_CUSTOMER_CHKPT <> v_cb30_cand_with_agt_mgr_visit then
    v_reconciled_fg := false;
  end if;
  
  if v_reconciled_fg = true then
    DBMS_OUTPUT.PUT_LINE('RESULT = reconciled');
  else
    DBMS_OUTPUT.PUT_LINE('RESULT = NOT RECONCILED');
  end if;


  DBMS_OUTPUT.PUT_LINE('-- Expected: Total Agent Manager Visits for exclusion:                   '||v_cb30_cand_with_agt_mgr_visit);
  DBMS_OUTPUT.PUT_LINE('-- Actual:   Total DRP Agent Manager Visits for exclusion:               '||v_WK_IC_DCA_CUSTOMER_CHKPT);

  DBMS_OUTPUT.PUT_LINE('');
  DBMS_OUTPUT.PUT_LINE('');

  
  --===============================================================================
  DBMS_OUTPUT.PUT_LINE('-- Legal Case Customers                                                  --');
  DBMS_OUTPUT.PUT_LINE('   (Total shown does not include LC customers in Zero Bal List / DCA)');
  DBMS_OUTPUT.PUT_LINE('');
  e_mess := 'reconcile legal case';
  v_reconciled_fg := true;

  begin
    select count(*)
    into v_WK_IC_LC_EXCLUSION
    from WK_IC_LC_EXCLUSION wk
    where wk.ice_customer_id in
    (
    select wk1.ice_customer_id 
    from cb26_cand_final_list_temp wk1
    )
    and wk.ice_customer_id not in
    (
    select ice_customer_id
    from cb_temp_wk_current_bal
    )
    and wk.ice_customer_id not in
    (
    select ice_customer_id
    from WK_IC_DCA_CUSTOMER_CHKPT
    )
    ;

    select count(*)
    into v_cb_LC_EXCLUSION
    from WK_IC_LC_EXCLUSION wk
    where wk.ice_customer_id in
    (
    select wk1.ice_customer_id 
    from cb26_cand_final_list_temp wk1
    )
    and wk.ice_customer_id not in
    (
    select ice_customer_id
    from cb22a_cand_without_bal_zero
    )
    and wk.ice_customer_id not in
    (
    select ice_customer_id
    from cb30_cand_with_agent_mgr_visit
    )
    ;

  EXCEPTION
    WHEN OTHERS
       THEN e_mess := SUBSTR(SQLERRM,1,80);
  END;
  
  -- check retrieved ...
  if v_WK_IC_LC_EXCLUSION <> v_cb_LC_EXCLUSION then
    v_reconciled_fg := false;
  end if;
  
  if v_reconciled_fg = true then
    DBMS_OUTPUT.PUT_LINE('RESULT = reconciled');
  else
    DBMS_OUTPUT.PUT_LINE('RESULT = NOT RECONCILED');
  end if;
  

  DBMS_OUTPUT.PUT_LINE('-- Expected: Total legal case customers in dormant list:                 '||v_cb_LC_EXCLUSION);
  DBMS_OUTPUT.PUT_LINE('-- Actual:   Total DRP LC customers removed from dormant list:           '||v_WK_IC_LC_EXCLUSION);

  DBMS_OUTPUT.PUT_LINE('');
  DBMS_OUTPUT.PUT_LINE('');


   --===============================================================================
  DBMS_OUTPUT.PUT_LINE('-- Reconcile Avoid Legal Action Accounts For Exclusion                   --');
  DBMS_OUTPUT.PUT_LINE('   (Total shown does not include LA customers in Zero Bal List / DCA / LC)');
  
  DBMS_OUTPUT.PUT_LINE('');
  e_mess := 'reconcile legal action';
  v_reconciled_fg := true;

  begin
    select count(*)
    into v_WK_IC_LACT_CUSTOMER_CHKPT
    from WK_IC_LACT_CUSTOMER_CHKPT wk
    where wk.ice_customer_id in
    (
    select wk1.ice_customer_id 
    from cb26_cand_final_list_temp wk1
    )
    and wk.ice_customer_id not in
    (
    select ice_customer_id
    from cb_temp_wk_current_bal
    )
    and wk.ice_customer_id not in
    (
    select ice_customer_id
    from WK_IC_DCA_CUSTOMER_CHKPT
    )
    and wk.ice_customer_id not in
    (
    select ice_customer_id
    from WK_IC_LC_EXCLUSION
    )
    ;

    SELECT count(distinct(ice_customer_id))
    into v_cb_LT_CUSTOMER
    FROM ice.legal_account_attributes laa
       , ice.account                  acc
    WHERE legal_flag_set = 'Y'
    AND acc.account_reference = laa.account_reference
    AND acc.key_type = 12
    and acc.ice_customer_id in
    (
    select wk1.ice_customer_id 
    from cb26_cand_final_list_temp wk1
    )
    and acc.ice_customer_id not in
    (
    select ice_customer_id
    from cb22a_cand_without_bal_zero
    )
    and acc.ice_customer_id not in
    (
    select ice_customer_id
    from cb30_cand_with_agent_mgr_visit
    )
    and acc.ice_customer_id not in
    (
    select ice_customer_id
    from WK_IC_LC_EXCLUSION
    )
   ;
  
  
  EXCEPTION
    WHEN OTHERS
       THEN e_mess := SUBSTR(SQLERRM,1,80);
  END;

  -- check retrieved ...
  if v_WK_IC_LACT_CUSTOMER_CHKPT <> v_cb_LT_CUSTOMER then
    v_reconciled_fg := false;
  end if;
  
  if v_reconciled_fg = true then
    DBMS_OUTPUT.PUT_LINE('RESULT = reconciled');
  else
    DBMS_OUTPUT.PUT_LINE('RESULT = NOT RECONCILED');
  end if;


  DBMS_OUTPUT.PUT_LINE('-- Expected: Total Legal Action Accounts for exclusion:                  '||v_cb_LT_CUSTOMER);
  DBMS_OUTPUT.PUT_LINE('-- Actual:   Total DRP Legal Action Accounts for exclusion:              '||v_WK_IC_LACT_CUSTOMER_CHKPT);

  DBMS_OUTPUT.PUT_LINE('');
  DBMS_OUTPUT.PUT_LINE('');

  
   --===============================================================================
  DBMS_OUTPUT.PUT_LINE('-- Reconcile Avoid Long Term Pay Accounts For Exclusion                  --');
  DBMS_OUTPUT.PUT_LINE('   (Total shown does not include LTP accounts in Zero Bal List / DCA / LC / LA)');
  
  DBMS_OUTPUT.PUT_LINE('');
  e_mess := 'reconcile legal action';
  v_reconciled_fg := true;

  begin
    select count(distinct(wk.ice_customer_id))
    into v_WK_IC_LONG_ARRANGEMENT_CHKPT
    from WK_IC_LONG_ARRANGEMENT_CHKPT wk
    where wk.ice_customer_id in
    (
    select wk1.ice_customer_id 
    from cb26_cand_final_list_temp wk1
    )
    and wk.ice_customer_id not in
    (
    select ice_customer_id
    from cb_temp_wk_current_bal
    )
    and wk.ice_customer_id not in
    (
    select ice_customer_id
    from WK_IC_DCA_CUSTOMER_CHKPT
    )
    and wk.ice_customer_id not in
    (
    select ice_customer_id
    from WK_IC_LC_EXCLUSION
    )
    and wk.ice_customer_id not in
    (
    select ice_customer_id
    from WK_IC_LACT_CUSTOMER_CHKPT
    )
    ;
    
    select count(distinct(acc.ice_customer_id))
    into v_cb_LTP_Accounts
    from ice.account acc
    where acc.ice_customer_id in
    (
    select wk1.ice_customer_id 
    from cb26_cand_final_list_temp wk1
    )
    and   acc.key_type          = 12
    and   exists
    (
    select *
    from ice.mv_account_pay_info mapi
    where mapi.account_num   = acc.account_reference
    and   mapi.last_payment_date between sysdate -91 and sysdate
    )
    and acc.ice_customer_id not in
    (
    select ice_customer_id
    from cb22a_cand_without_bal_zero
    )
    and acc.ice_customer_id not in
    (
    select ice_customer_id
    from cb30_cand_with_agent_mgr_visit
    )
    and acc.ice_customer_id not in
    (
    select ice_customer_id
    from WK_IC_LC_EXCLUSION
    )
    and acc.ice_customer_id not in
    (
    select ice_customer_id
    from WK_IC_LACT_CUSTOMER_CHKPT
    )
   ;
  
  
  EXCEPTION
    WHEN OTHERS
       THEN e_mess := SUBSTR(SQLERRM,1,80);
  END;

  -- check retrieved ...
  if v_WK_IC_LONG_ARRANGEMENT_CHKPT <> v_cb_LTP_Accounts then
    v_reconciled_fg := false;
  end if;
  
  if v_reconciled_fg = true then
    DBMS_OUTPUT.PUT_LINE('RESULT = reconciled');
  else
    DBMS_OUTPUT.PUT_LINE('RESULT = NOT RECONCILED');
  end if;


  DBMS_OUTPUT.PUT_LINE('-- Expected: Total Long Term Pay Accounts for exclusion:                 '||v_cb_LTP_Accounts);
  DBMS_OUTPUT.PUT_LINE('-- Actual:   Total DRP Long Term Pay Accounts for exclusion:             '||v_WK_IC_LONG_ARRANGEMENT_CHKPT);

  DBMS_OUTPUT.PUT_LINE('');
  DBMS_OUTPUT.PUT_LINE('');

  
   --===============================================================================
  DBMS_OUTPUT.PUT_LINE('-- Reconcile Historic Preservation For Exclusion                  --');
  DBMS_OUTPUT.PUT_LINE('   (Total shown does not include HP accounts in Zero Bal List / DCA / LC / LA / LongTerm)');
  
  DBMS_OUTPUT.PUT_LINE('');
  e_mess := 'reconcile legal action';
  v_reconciled_fg := true;

  begin
    select count(distinct(wk.ice_customer_id))
    into v_WK_IC_LACT_CUST_HIST_CHKPT
    from WK_IC_LACT_CUST_HIST_CHKPT wk
    where wk.ice_customer_id in
    (
    select wk1.ice_customer_id 
    from cb26_cand_final_list_temp wk1
    )
    and wk.ice_customer_id not in
    (
    select ice_customer_id
    from cb_temp_wk_current_bal
    )
    and wk.ice_customer_id not in
    (
    select ice_customer_id
    from WK_IC_DCA_CUSTOMER_CHKPT
    )
    and wk.ice_customer_id not in
    (
    select ice_customer_id
    from WK_IC_LC_EXCLUSION
    )
    and wk.ice_customer_id not in
    (
    select ice_customer_id
    from WK_IC_LACT_CUSTOMER_CHKPT
    )
    and wk.ice_customer_id not in
    (
    select ice_customer_id
    from WK_IC_LONG_ARRANGEMENT_CHKPT
    )
    ;
    
    select count(distinct(acc.ice_customer_id))
    into v_cb_Hist_laa_Accounts
    from legal_account_attributes laa, account acc
    where legal_flag_set = 'N'
    and   nvl(last_update,TO_DATE('31-DEC-9999')) >=  ADD_MONTHS(to_date('10-APR-2018'), -72)
    and   acc.account_reference = laa.account_reference
	and   acc.key_type = 12
    --
    and   acc.ice_customer_id in
    (
    select wk1.ice_customer_id 
    from cb26_cand_final_list_temp wk1
    )
     and acc.ice_customer_id not in
    (
    select ice_customer_id
    from cb22a_cand_without_bal_zero
    )
    and acc.ice_customer_id not in
    (
    select ice_customer_id
    from cb30_cand_with_agent_mgr_visit
    )
    and acc.ice_customer_id not in
    (
    select ice_customer_id
    from WK_IC_LC_EXCLUSION
    )
    and acc.ice_customer_id not in
    (
    select ice_customer_id
    from WK_IC_LACT_CUSTOMER_CHKPT
    )
    and acc.ice_customer_id not in
    (
    select ice_customer_id
    from WK_IC_LONG_ARRANGEMENT_CHKPT
    )
   ;
  
  
  EXCEPTION
    WHEN OTHERS
       THEN e_mess := SUBSTR(SQLERRM,1,80);
  END;

  -- check retrieved ...
  if v_WK_IC_LONG_ARRANGEMENT_CHKPT <> v_cb_LTP_Accounts then
    v_reconciled_fg := false;
  end if;
  
  if v_reconciled_fg = true then
    DBMS_OUTPUT.PUT_LINE('RESULT = reconciled');
  else
    DBMS_OUTPUT.PUT_LINE('RESULT = NOT RECONCILED');
  end if;


  DBMS_OUTPUT.PUT_LINE('-- Expected: Total Historic Preservation Accounts for exclusion:         '||v_cb_Hist_laa_Accounts);
  DBMS_OUTPUT.PUT_LINE('-- Actual:   Total DRP Historic Preservation Accounts for exclusion:     '||v_WK_IC_LACT_CUST_HIST_CHKPT);

  DBMS_OUTPUT.PUT_LINE('');
  DBMS_OUTPUT.PUT_LINE('');

  
   --===============================================================================
  DBMS_OUTPUT.PUT_LINE('-- Reconcile Orphaned Accts For Exclusion                  --');
  DBMS_OUTPUT.PUT_LINE('   (Total shown does not include Orphaned accounts in Zero Bal List / DCA / LC / LA / LongTerm / HP)');
  
  DBMS_OUTPUT.PUT_LINE('');
  e_mess := 'reconcile legal action';
  v_reconciled_fg := true;

  begin
    select count(distinct(wk.ice_customer_id))
    into v_WK_IC_ORPHAN_ACCOUNT_CHKPT
    from WK_IC_ORPHAN_ACCOUNT_CHKPT wk
    where wk.ice_customer_id in
    (
    select wk1.ice_customer_id 
    from cb26_cand_final_list_temp wk1
    )
    and wk.ice_customer_id not in
    (
    select ice_customer_id
    from cb_temp_wk_current_bal
    )
    and wk.ice_customer_id not in
    (
    select ice_customer_id
    from WK_IC_DCA_CUSTOMER_CHKPT
    )
    and wk.ice_customer_id not in
    (
    select ice_customer_id
    from WK_IC_LC_EXCLUSION
    )
    and wk.ice_customer_id not in
    (
    select ice_customer_id
    from WK_IC_LACT_CUSTOMER_CHKPT
    )
    and wk.ice_customer_id not in
    (
    select ice_customer_id
    from WK_IC_LONG_ARRANGEMENT_CHKPT
    )
    and wk.ice_customer_id not in
    (
    select ice_customer_id
    from WK_IC_LACT_CUST_HIST_CHKPT
    )
    ;
    
    select count(*)
    into v_cb31_custs_with_orphan_accts
    from cb31_custs_with_orphan_accts cb
    --
    where   cb.ice_customer_id in
    (
    select wk1.ice_customer_id 
    from cb26_cand_final_list_temp wk1
    )
     and cb.ice_customer_id not in
    (
    select ice_customer_id
    from cb22a_cand_without_bal_zero
    )
    and cb.ice_customer_id not in
    (
    select ice_customer_id
    from cb30_cand_with_agent_mgr_visit
    )
    and cb.ice_customer_id not in
    (
    select ice_customer_id
    from WK_IC_LC_EXCLUSION
    )
    and cb.ice_customer_id not in
    (
    select ice_customer_id
    from WK_IC_LACT_CUSTOMER_CHKPT
    )
    and cb.ice_customer_id not in
    (
    select ice_customer_id
    from WK_IC_LONG_ARRANGEMENT_CHKPT
    )
    and cb.ice_customer_id not in
    (
    select ice_customer_id
    from WK_IC_LACT_CUST_HIST_CHKPT
    )
   ;
  
  
  EXCEPTION
    WHEN OTHERS
       THEN e_mess := SUBSTR(SQLERRM,1,80);
  END;

  -- check retrieved ...
  if v_WK_IC_LONG_ARRANGEMENT_CHKPT <> v_cb_LTP_Accounts then
    v_reconciled_fg := false;
  end if;
  
  if v_reconciled_fg = true then
    DBMS_OUTPUT.PUT_LINE('RESULT = reconciled');
  else
    DBMS_OUTPUT.PUT_LINE('RESULT = NOT RECONCILED');
  end if;


  DBMS_OUTPUT.PUT_LINE('-- Expected: Total Orphaned Accounts for exclusion:                      '||v_cb31_custs_with_orphan_accts);
  DBMS_OUTPUT.PUT_LINE('-- Actual:   Total DRP Orphaned Accounts for exclusion:                  '||v_WK_IC_ORPHAN_ACCOUNT_CHKPT);

  DBMS_OUTPUT.PUT_LINE('');
  DBMS_OUTPUT.PUT_LINE('');

  
--*******************************************************************************************************
--*******************************************************************************************************
--*******************************************************************************************************

  DBMS_OUTPUT.PUT_LINE('==============================================================================');
  DBMS_OUTPUT.PUT_LINE('====== Dormancy List Totals                                             ======');
  DBMS_OUTPUT.PUT_LINE('==============================================================================');
  DBMS_OUTPUT.PUT_LINE('');

  DBMS_OUTPUT.PUT_LINE('-- Reconcile Dormant List --');

  DBMS_OUTPUT.PUT_LINE('');
  e_mess := 'reconcile dormant list';
  v_reconciled_fg := true;

  begin
    select count(*)
    into v_wk_ic_dormant_list
    from drp.wk_ic_dormant_list dl
    ;

    select count(*) into v_cb27_cand_final_list from cb27_cand_final_list;
  EXCEPTION
    WHEN OTHERS
       THEN e_mess := SUBSTR(SQLERRM,1,80);
  END;

  -- check retrieved ...
  if v_wk_ic_dormant_list <> v_cb27_cand_final_list then
    v_reconciled_fg := false;
  end if;
  
  if v_reconciled_fg = true then
    DBMS_OUTPUT.PUT_LINE('RESULT = reconciled');
  else
    DBMS_OUTPUT.PUT_LINE('RESULT = NOT RECONCILED');
  end if;


  DBMS_OUTPUT.PUT_LINE('-- Expected: Total Calculated Dormant List:                              '||v_cb27_cand_final_list);
  DBMS_OUTPUT.PUT_LINE('-- Actual:   Total DRP Dormant List:                                     '||v_wk_ic_dormant_list);

  DBMS_OUTPUT.PUT_LINE('');
  DBMS_OUTPUT.PUT_LINE(''); 
  
END; 
/
  /*
  --==============================================================================
  DBMS_OUTPUT.PUT_LINE('====== Reconcile Shell For Exclusion ======');
  --==============================================================================
  DBMS_OUTPUT.PUT_LINE('');
  e_mess := 'reconcile shell';
  v_reconciled_fg := true;

  begin
    select count(*) 
    into v_wk_ic_shell_account_chkpt
    from wk_ic_shell_account_chkpt wk1, wk_ic_age_chkpt wk2 
    where wk1.ice_customer_id = wk2.ice_customer_id
    --and   wk2.change_time < ADD_MONTHS(SYSDATE, -85);
    ;
    select count(*) into v_cb3_cand_with_shell from cb3_cand_with_shell;
  EXCEPTION
    WHEN OTHERS
       THEN e_mess := SUBSTR(SQLERRM,1,80);
  END;

  -- check retrieved ...
  if v_wk_ic_shell_account_chkpt <> v_cb3_cand_with_shell then
    v_reconciled_fg := false;
  end if;
  
  if v_reconciled_fg = true then
    DBMS_OUTPUT.PUT_LINE('RESULT = reconciled');
  else
    DBMS_OUTPUT.PUT_LINE('RESULT = NOT RECONCILED');
  end if;


  DBMS_OUTPUT.PUT_LINE('-- Expected: Total Shell retrieved lt retention date:          '||v_cb3_cand_with_shell);
  DBMS_OUTPUT.PUT_LINE('-- Actual:   Total DRP Shell lt retention  date:               '||v_wk_ic_shell_account_chkpt);

  DBMS_OUTPUT.PUT_LINE('');
  DBMS_OUTPUT.PUT_LINE('');

  --==============================================================================
  DBMS_OUTPUT.PUT_LINE('====== Reconcile Orphaned For Exclusion ======');
  --==============================================================================
  DBMS_OUTPUT.PUT_LINE('');
  e_mess := 'reconcile orphaned';
  v_reconciled_fg := true;

  begin
    select count(*) into v_wk_ic_orphan_account_chkpt
    from wk_ic_orphan_account_chkpt wk1, wk_ic_age_chkpt wk2 
    where wk1.ice_customer_id = wk2.ice_customer_id
    ;
    select count(*) into v_cb6_cand_with_orphaned from cb6_cand_with_orphaned;
  EXCEPTION
    WHEN OTHERS
       THEN e_mess := SUBSTR(SQLERRM,1,80);
  END;

  -- check retrieved ...
  if v_wk_ic_orphan_account_chkpt <> v_cb6_cand_with_orphaned then
    v_reconciled_fg := false;
  end if;
  
  if v_reconciled_fg = true then
    DBMS_OUTPUT.PUT_LINE('RESULT = reconciled');
  else
    DBMS_OUTPUT.PUT_LINE('RESULT = NOT RECONCILED');
  end if;


  DBMS_OUTPUT.PUT_LINE('-- Expected: Total Orphaned retrieved lt retention date:       '||v_cb6_cand_with_orphaned);
  DBMS_OUTPUT.PUT_LINE('-- Actual:   Total DRP Orphaned lt retention  date:            '||v_wk_ic_orphan_account_chkpt);

  DBMS_OUTPUT.PUT_LINE('');
  DBMS_OUTPUT.PUT_LINE('');
*/


